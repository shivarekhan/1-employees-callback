/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require('fs');
const path = require('path');

const dataPath = path.join(__dirname, "./data.json");

//create json file
function createJsonFIle(jsonFIlePath, jsonData,callbacks) {
    fs.writeFile(jsonFIlePath, jsonData, function (error) {
        if (error) {
            console.log(`FIle ${jsonFIlePath} could't created `);
            console.log(err);
            callbacks(error)
        }
    })
}

//Retrieve data for ids : [2, 13, 23].

function retiveDataForGIvenId(callbacks) {

    const id = [2, 13, 23];

    fs.readFile(dataPath, 'utf8', function (error, data) {

        if (error) {
            console.log(`ERROR : Error in file read .`, error);
            callbacks(error);
        } else {
            const parseData = JSON.parse(data);
            const dataForGivenId = parseData["employees"].filter(function (current) {
                if (id.includes(current["id"])) {
                    return current;
                }
            }, []);

            const jsonFIlePath = path.join(__dirname, "./dataForGivenIds.json");
            const dataForGivenIdString = JSON.stringify(dataForGivenId);

            createJsonFIle(jsonFIlePath, dataForGivenIdString, function (error) {
                if (error) {
                    callbacks(error);
                }
            });
            callbacks();

        }
    });
}
//Group data based on companies.
// { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

function groupDataByCompanies(callbacks) {

    fs.readFile(dataPath, 'utf8', function (error, data) {

        if (error) {
            console.log(`Can't read ${dataPath}`);
            console.log(error);
            callbacks(error);
        } else {
            const parseData = JSON.parse(data);
            const dataGroupedByCompany = parseData["employees"].reduce(function (groupedList, current) {
                if (!groupedList[current.company]) {
                    groupedList[current.company] = [];
                    groupedList[current.company].push(current);
                } else {
                    groupedList[current.company].push(current);
                }
                return groupedList;
            }, {});

            const jsonFIlePath = path.join(__dirname, "./groupedDataByCompany.json");
            const dataGroupedByCompanyString = JSON.stringify(dataGroupedByCompany);
            createJsonFIle(jsonFIlePath, dataGroupedByCompanyString,function(error){
                if(error){
                    callbacks(error);
                }
            });
            callbacks();
        }
    })
}

//Get all data for company Powerpuff Brigade
function powerPuffBrigadeData(callbacks) {
    const companyDataPath = path.join(__dirname, "./groupedDataByCompany.json");

    fs.readFile(companyDataPath, 'utf8', function (error, data) {
        if (error) {

            console.log(`ERROR : file ${companyDataPath} read failed`);
            console.log(error);
            callbacks(error);

        } else {

            const parseData = JSON.parse(data);
            const dataForPowerPuffBrigade = Object.entries(parseData).filter(function (current) {
                if (current[0] === "Powerpuff Brigade") {
                    return current;
                }
            });

            const powerPuffDataString = JSON.stringify(dataForPowerPuffBrigade);
            powerPuffJsonPath = path.join(__dirname, "./powerPuffData.json");
            createJsonFIle(powerPuffJsonPath, powerPuffDataString,function(error){
                if(error){
                    callbacks(error);
                }
            });
            callbacks();

        }
    });
}

// 4. Remove entry with id 2.
function removeEntryForGivenId(callbacks, id = 2) {
    fs.readFile(dataPath, 'utf8', function (error, data) {
        if (error) {
            console.log(`ERROR : file ${dataPath} read unsuccessfull`);
            console.log(error);
            callbacks(error);
        } else {
            const parseData = JSON.parse(data);
            const dataWithoutGivenId = parseData["employees"].filter(function (current) {
                if (current.id !== 2) {
                    return true;
                }
            }, []);

            const dataWithoutGivenIdString = JSON.stringify(dataWithoutGivenId);
            const dataWithoutGivenIdPath = path.join(__dirname, "./dataWithoutGivenId.json");

            createJsonFIle(dataWithoutGivenIdPath, dataWithoutGivenIdString,function(error){
                if(error){
                    callbacks(error);
                }
            });
            callbacks();
        }
    });
}


// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
function sortData() {

    fs.readFile(dataPath, 'utf-8', function (error, data) {
        if (error) {
            console.log(error);
            callbacks(error);
        } else {
            const parseData = JSON.parse(data);
            const sortData = parseData["employees"].sort(function (data1, data2) {
                if (data1.company == data2.company) {
                    return data1.id - data2.id;
                } else {
                    return data1.company < data2.company ? 1 : -1;
                }
            });
            const sortedDataString = JSON.stringify(sortData);
            const sortedDataPath = path.join(__dirname, "./sortedData.json");

            createJsonFIle(sortedDataPath, sortedDataString,function(error){
                if(error){
                    callbacks(error);
                }
            });

        }
    })
}



// driver
retiveDataForGIvenId(function (error) {
    if (error) {
        console.log(error);
    } else {
        groupDataByCompanies(function (error) {
            if (error) {
                console.log(error);
            } else {
                powerPuffBrigadeData(function (error) {
                    if (error) {
                        console.log(error)
                    } else {
                        removeEntryForGivenId(function (error) {
                            if (error) {
                                console.log(error);
                            } else {
                                sortData(function (error) {
                                    if (error) {
                                        console.log(error);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

